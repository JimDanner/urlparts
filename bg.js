'use strict';

browser.tabs.onActivated.addListener(e => browser.pageAction.show(e.tabId));
browser.tabs.onUpdated.addListener(t => browser.pageAction.show(t));

