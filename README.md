# Analyze address/URL
Firefox extension that adds a button into the address bar. By clicking it, you get to see the details of the current address (a.k.a. *URL*). Also, it shows the *query parameters* that are part of the URL.

Further usage:

* Click any item to copy its text to the system clipboard.
* Click the *Copy all* button to copy all relevant information
* Click the *Address from clipboard* button to analyze a web address you have just copied somehwere, for example when you right-clicked a link on a page and selected 'Copy Link'.