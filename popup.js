'use strict';

/* 1. parameters */
const MSG = browser.i18n.getMessage;
const urlPartNames = {  // officialname: Firefoxname
	scheme: "protocol",
	username: "username",
	password: "password",
	host: "hostname",
	port: "port",
	path: "pathname",
	query: "search",
	fragment: "hash"
};
const explanationPartStrings = ['\n'+MSG('scheme')+':', '//',
	MSG('username'), ':'+MSG('password'), '@', MSG('host'), ':'+MSG('port'),
	MSG('path'), '?'+MSG('query'), '#'+MSG('fragment'), '\n'
];
const list = document.querySelector('.list'),
      expl = document.querySelector('.explanation'),
      foot = document.querySelector('.footer'),
      copyall = document.getElementById('copyall'),
      fromcb = document.getElementById('fromcb');
let textOutput = '', writePerm, readPerm;

/* 2. build up the popup content */
list.firstElementChild.textContent = MSG("popupTitle");
copyall.textContent = MSG("copyAll");
fromcb.textContent = MSG("fromClipboard");

Promise.all([
	browser.tabs.query({active: true, currentWindow: true}),
	browser.permissions.getAll()
]).then(([currentTab, perm]) => {
	if (!currentTab.length) return;
	[writePerm, readPerm] =
		["clipboardWrite", "clipboardRead"].map(p => perm.permissions.includes(p));
	if (writePerm || readPerm) {
		foot.classList.remove('hidden');
		if (!writePerm) {
			copyall.classList.add('disabled');
			copyall.title = MSG("requiresPermission");
		}
		if (!readPerm) {
			fromcb.classList.add('disabled');
			fromcb.title = MSG("requiresPermission");
		}
	}
	buildPartsTable(currentTab[0].url);
});

function buildPartsTable(url) {
	try {
		var parser = new URL(url);
	} catch(err) {
		console.warn(`"${url}" is rejected for URL analysis`);
		return false;
	}
	textOutput = url + '\n';
	let tooltipText = MSG("clickToCopy") + (writePerm ? '' : '\n'+MSG("requiresPermission"));

	/* Restore beginning state of the popup */
	list.querySelectorAll('.part, .queryhead').forEach(p => p.remove());
	for (let sp of expl.children)
		sp.classList.add('off');
	
	function addDescriptionAndValue(value, description) {
		let descr = newElement('div', ["part", "description"], null,
			description, {title: tooltipText}); 
		let text = newElement('div', ["part", "value"], null,
			value.replace(/\n/g, '⤶').replace(/\r/g, ''),
			{title: tooltipText, origVal: value});
		list.append(descr, text);			
		textOutput += description + '\t' + value + '\n';	
	}
	
	/* Parse it and put the parts on the page */
	for (let p in urlPartNames) {
		if (!parser[urlPartNames[p]]) continue;

		expl.querySelectorAll('.'+p).forEach(el => el.classList.remove('off'));
		expl.childNodes.forEach((node, index) => {
			node.textContent = explanationPartStrings[index];
		});
		
		let content = decodeURIComponent(parser[urlPartNames[p]]);
		if (p === 'scheme') content = content.slice(0, -1);
		else if (p === 'query' || p === 'fragment') content = content.slice(1);
		else if (p === 'password') content = '•'.repeat(content.length);
		addDescriptionAndValue(content, MSG(p));
	}
	if (!parser.hostname && url.match(/^[^:]+:\/\/\//))  // mainly file:///
		expl.querySelector('.host').classList.remove('off');  // affects '//'

	/* Query parameters table */
	if (parser.search) {
		list.append(newElement('div', ["heading", "queryhead"], null, MSG("queryParamTitle")));
		textOutput += MSG("queryParamTitle") + ':\n';	
		parser.searchParams.forEach(addDescriptionAndValue);
	}

	return true;
}

/* 3. add the CSS for theme colors */
browser.theme.getCurrent().then(th => {
	if (!th.colors) return;
	if (!th.colors.popup || !th.colors.popup_text) return;
	if (!th.colors.button_hover && !th.colors.button_background_hover) return;
	// only do non-default colors if they are ALL available
	document.styleSheets[0].insertRule(`
		:root {
			--theme-bg-color: ${th.colors.popup};
			--theme-hover-color: ${th.colors.button_hover || th.colors.button_background_hover};
			--theme-txt-color: ${th.colors.popup_text};
			--theme-shortcut-color: ${th.colors.popup_text};
		}`, 2);
});

/* 4. set event listeners for clicks on the popup */
list.addEventListener('click', c => {
	if (c.target.classList.contains('part')) {
		navigator.clipboard.writeText(c.target.origVal || c.target.textContent);
		window.close();
	}
});
copyall.addEventListener('click', () => {
	navigator.clipboard.writeText(textOutput).then(() => window.close());
});
fromcb.addEventListener('click', async () => {
	let url = await navigator.clipboard.readText();
	if (!buildPartsTable(url)) {
		if (url.length > 35) url = url.slice(0,33) + '...';
		expl.append(newElement('p', [], null, MSG("notValid", url)));
		/* No need to delete after use: user will close popup to copy something else */
	}
});

/* UTILITY FUNCTION */
function newElement(tag, classes, attributes, content, otherProperties) {
	let element = document.createElement(tag);
	element.classList.add(...classes);
	if (attributes) for (let a in attributes) element.setAttribute(a, attributes[a]);
	return Object.assign(element, {textContent: content || ''}, otherProperties);
}
