# Name of the file to make; files to compile
RESULT = analyzeaddress@jimdanner.xpi
SOURCES = .
EXCLUDE = _* .* '**/.*' '.git/*' 'doc/*' README.md Makefile

# Compilation parameters
CC = zip
CFLAGS = -r
EXCLUDEFLAG = -x

# Targets
all: clean compress

compress:
	$(CC) $(CFLAGS) $(RESULT) $(SOURCES) $(EXCLUDEFLAG) $(EXCLUDE)

clean:
	rm -f $(RESULT)
