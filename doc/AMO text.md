Text on [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/analyze-address-url/) (AMO)

# Analyze address/URL
Click an address bar button and see how the address (URL) is built up, including a list of query parameters. Click an item to copy it. Uses the new Firefox 'proton' style, responsive to Dark mode and Themes.


## Detail

Firefox extension that adds a button into the address bar. By clicking it, you get to see the **details of the current address** (a.k.a. *URL*). Also, it shows the **query parameters** that are part of the URL: often a series of data like `utm_campaign=...`.

Further functions:

* Click any item to copy its text to the system clipboard
* Click the *Copy all* button to copy all relevant information
* Click the *Address from clipboard* button to analyze a web address you have just copied from somehwere, for example when you right-clicked a link on a page and selected 'Copy Link'